# Observing Capabilities

This repo contains [Sphinx](https://www.sphinx-doc.org) documentation for observing capabilities of observing runs.

The documentation is visible at https://observing.docs.ligo.org/plan/

## Stroke-by-stroke Editing

* edit index.rst
* comment, and commit changes
* click on rocket
* click on passed
* click on html
* MUST TAG to publish
    * go to Plan, then Repository, then Tags
    * New Tag upper right
    * Follow tag naming protocol — must start with a v, so e.g., v2022.05.13 — look carefully at what is typed...
    * Create tag
* Look at public posting (https://observing.docs.ligo.org/plan/#); takes a couple of minutes (not seconds)


## Editing

- Make your edits (in a new branch or directly to master) on the file `index.rst`.
- Push to origin
- The Gitlab CI pipeline will build draft HTML pages automatically every time you push a commit. The draft will not be made public.
- The peek the draft:
    - Use the rocketship icon in the menu to find [CI/CI > Pipelines](https://git.ligo.org/observing/plan/-/pipelines)
    - Click the `passed` button associated with the most recent commit, then click the `html` button.
    - On the `html` job, under `Job artifacts` select the `Browse` button.
    - Navigate to `html` and open `index.html` to view the page draft in a new tab.

## Publish to public URL

To publish a new version, we only need to tag the commit with a version number.

Ideally, this tag number should match the one in the `conf.py` file, so please remember to bump the version number on the `release` variable as well.

e.g.

```python
# conf.py
release = '2022.4'
```

Tags can be made locally and pushed to origin:

```bash
git tag v2022.4
git push origin --tags
```

OR [create a tag on the web interface](https://git.ligo.org/observing/plan/-/tags) at `Repository > Tags > New tag`.

The page will deploy to the public URL automatically.
