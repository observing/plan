LIGO, VIRGO AND KAGRA OBSERVING RUN PLANS
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. only:: html

    .. toctree::
       :hidden:

       glossary




(15 February 2025 update; next update 15 March 2025 or sooner)

**We remind of a revision to the schedule for the remainder of the O4 observing run**.  The change includes a break in observing for maintenance and commissioning work, and an extension of the run, as follows: 

* Maintenance/commissioning break from 1 April 2025 at 15:00 UTC to 4 June 2025 at 15:00 UTC
* O4 will end on 7 October 2025 at 15:00 UTC

The April/May break in observing is prompted by the importance of repairing a faulty beam tube gate valve at LIGO Livingston site before this year’s hurricane season and other work on the other detectors of the network that will reduce risk related to the O4-O5 transition. The extension of O4 to early October yields approximately the same amount of calendar observing time as was planned to obtain with the previous June 2025 end date, after accounting for the time lost from this April/May 2025 break, plus that lost from unplanned interruptions in 2024 (shutter failure at LIGO Livingston; output Faraday damage and laser glitch issues at LIGO Hanford).

The LIGO Livingston detector has been observing with a BNS range around 170 Mpc, and a duty cycle of 80%. LIGO Hanford has been observing at around 160 Mpc, and a duty cycle of around 65%.


Virgo continues observing in O4b, with a duty cycle around 70% and a 50-55 Mpc BNS range. Currently, the Virgo Collaboration is reassessing its plans for O5 and both the entry date into O5 and the target sensitivity are unclear. This work is progressing and we expect to be able to define our plans for O5 in the first half of 2025.


KAGRA continues noise hunting and cooling the main mirrors. The laser input to the main interferometer is planned to be increased to 10 W to reduce shot noise, in order to improve sensitivity in the high-frequency range. We expect to rejoin the observing run before the end of O4 with a sensitivity of around 10 Mpc.

The planned end date for O4 is 7 October 2025 at 15:00 UTC. LIGO, Virgo and KAGRA (LVK) are continually assessing planned commissioning and upgrade activities. Should there be any changes to the current planning, we would communicate them here as soon as they are adopted by the LVK.


For more information, see:

OpenMMA wiki - https://github.com/scimma/openMMA/wiki
IGWN public alert user guide - https://emfollow.docs.ligo.org/userguide/



**Timeline**
--------

The gravitational-wave observing schedule is divided into Observing Runs, down time for construction and commissioning, and transitional Engineering Runs between commissioning and observing runs. The current best understanding of the long-term observing schedule is shown
below (or https://dcc.ligo.org/LIGO-G2002127/public). Since BNS (Binary Neutron Star) mergers are a well-studied class of gravitational-wave signals, this figure gives the BNS range for for a single-detector SNR threshold of 8 in each observing run.

.. figure:: _static/ObsScen_timeline.png
   :alt: Long-term observing schedule




The O5 start dates, duration, and sensitivities are current best guesses, and will likely be adjusted as we approach that run for all the detectors (see text). 


Live Status
-----------

A public web page that report live status of the
LIGO/Virgo detectors and alert infrastructure is at

*  | **Detector Status Portal**: Daily summary of detector performance.
   |  https://online.igwn.org

   
\ 
\ 
   -- Page Design adapted from the LIGO/Virgo/KAGRA Public Alerts User Guide --





